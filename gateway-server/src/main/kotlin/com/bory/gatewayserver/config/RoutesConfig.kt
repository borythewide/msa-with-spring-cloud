package com.bory.gatewayserver.config

import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.cloud.gateway.route.builder.filters
import org.springframework.cloud.gateway.route.builder.routes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RoutesConfig {
    @Bean
    fun routes(builder: RouteLocatorBuilder): RouteLocator =
            builder.routes {
                route {
                    path("/first/**")
                    filters {
                        removeRequestHeader("Cookie")
                        rewritePath("/first/((?<segment>.*))", "/\${segment}")
                        addRequestHeader("X-Gateway-Bypass", "to-first-service")
                        hystrix { it.setName("hystrix").setFallbackUri("forward:/fallback/first-service") }
                    }
                    uri("lb://first-service/")
                }
                route {
                    path("/second/**")
                    filters {
                        removeRequestHeader("Cookie")
                        rewritePath("/second/((?<segment>.*))", "/\${segment}")
                        addRequestHeader("X-Gateway-Bypass", "to-second-service")
                        hystrix { it.setName("hystrix").setFallbackUri("forward:/fallback/second-service") }
                    }
                    uri("lb://second-service/")
                }
                route {
                    path("/auth/**")
                    filters {
                        removeRequestHeader("Cookie")
                        rewritePath("/auth/((?<segment>.*))", "/auth/\${segment}")
                        addRequestHeader("X-Gateway-Bypass", "to-auth-service")
                        hystrix { it.setName("hystrix").setFallbackUri("forward:/fallback/auth-service") }
                    }
                    uri("lb://authorization-server/")
                }
            }
}