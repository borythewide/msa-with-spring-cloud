package com.bory.gatewayserver.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/fallback")
class FallbackController {
    @GetMapping("/first-service")
    fun fallbackForFirstService() = mapOf<String, String>("message" to "First-Service-Error")

    @GetMapping("/second-service")
    fun fallbackForSecondService() = mapOf<String, String>("message" to "Second-Service-Error")

    @GetMapping("/auth-service")
    fun fallbackForAuthService() = mapOf<String, String>("message" to "Auth-Service-Error")

}