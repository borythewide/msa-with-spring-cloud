package com.bory.common.config

import com.bory.common.filter.UserContextFilter
import com.bory.common.interceptor.UserContextInterceptor
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class WebConfig {
    @Bean
    fun oauthRestTemplate(): RestTemplate = RestTemplateBuilder().build().apply {
        interceptors.add(UserContextInterceptor())
    }

    @Bean
    fun restTemplate(): RestTemplate = RestTemplateBuilder().build()

    @Bean
    fun userContextFilter(): UserContextFilter = UserContextFilter()
}