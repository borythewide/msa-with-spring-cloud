package com.bory.common.filter

import com.bory.common.context.UserContext
import com.bory.common.context.UserContextHolder
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class UserContextFilter : Filter {
    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(UserContextFilter::class.java)
    }

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val httpServletRequest = request as HttpServletRequest
        val userContext = UserContextHolder.getContext().apply {
            authorization = httpServletRequest.getHeader(UserContext.AUTHORIZATION)
            correlationId = httpServletRequest.getHeader(UserContext.CORRELATION_ID) ?: UUID.randomUUID().toString()
        }

        LOGGER.debug("UserContextFilter authorization = ${userContext.authorization}")
        LOGGER.debug("UserContextFilter correlationId = ${userContext.correlationId}")

        chain.doFilter(httpServletRequest, response)
    }

}