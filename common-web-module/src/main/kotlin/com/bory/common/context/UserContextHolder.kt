package com.bory.common.context

import kotlin.concurrent.getOrSet

class UserContextHolder {
    companion object {
        private val userContextThreadLocal: ThreadLocal<UserContext> = ThreadLocal()

        fun getContext() = userContextThreadLocal.getOrSet { UserContext() }

        fun setContext(context: UserContext) = userContextThreadLocal.set(context)
    }
}