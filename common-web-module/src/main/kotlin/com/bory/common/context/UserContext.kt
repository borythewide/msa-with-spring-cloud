package com.bory.common.context

data class UserContext(var authorization: String = "",
                       var correlationId: String = "") {
    companion object {
        const val AUTHORIZATION: String = "Authorization"
        const val CORRELATION_ID: String = "Corr-ID"
    }
}

