package com.bory.common.interceptor

import com.bory.common.context.UserContext
import com.bory.common.context.UserContextHolder
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse

class UserContextInterceptor : ClientHttpRequestInterceptor {
    override fun intercept(request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution): ClientHttpResponse = with(request.headers) {
        add(UserContext.AUTHORIZATION, UserContextHolder.getContext().authorization)
        add(UserContext.CORRELATION_ID, UserContextHolder.getContext().correlationId)

        execution.execute(request, body)
    }
}