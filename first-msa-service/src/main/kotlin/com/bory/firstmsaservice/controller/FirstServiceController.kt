package com.bory.firstmsaservice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.security.Principal

@RestController
class FirstServiceController(
        @Autowired @Qualifier("oauthRestTemplate") val oauthRestTemplate: RestTemplate,
        @Value("\${api-gateway.url}") val apiGatewayUrl: String
) {
    @GetMapping("/api/hello-first-world")
    fun helloFirstWorld(principal: Principal) = mapOf(
            "message" to "hello-first-world",
            "principal" to principal
    )

    @GetMapping("/api/hello-second-world")
    fun helloSecondWorld(principal: Principal): Map<String, Any> {
        val responseMap = oauthRestTemplate.getForEntity("$apiGatewayUrl/second/api/hello-second-world", Map::class.java)

        return mapOf(
                "message" to "hello-first-second-world",
                "payload" to responseMap
        )
    }
}