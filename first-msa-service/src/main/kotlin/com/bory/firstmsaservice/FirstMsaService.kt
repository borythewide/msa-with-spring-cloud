package com.bory.firstmsaservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication(scanBasePackages = ["com.bory.common", "com.bory.firstmsaservice"])
@EnableEurekaClient
class FirstMsaService

fun main(args: Array<String>) {
    runApplication<FirstMsaService>(*args)
}