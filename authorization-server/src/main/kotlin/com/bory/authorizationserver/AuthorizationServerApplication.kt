package com.bory.authorizationserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@EnableEurekaClient
@RestController
class AuthorizationServerApplication {
    @RequestMapping(value = ["/user"], produces = ["application/json"])
    fun user(user: OAuth2Authentication) = mapOf<String, Any>(
            "user" to user.userAuthentication.principal,
            "authorities" to AuthorityUtils.authorityListToSet(user.userAuthentication.authorities)
    )
}

fun main(args: Array<String>) {
    runApplication<AuthorizationServerApplication>(*args)
}
