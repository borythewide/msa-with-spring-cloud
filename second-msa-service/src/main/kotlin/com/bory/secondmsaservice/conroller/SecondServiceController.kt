package com.bory.secondmsaservice.conroller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
class SecondServiceController {
    @GetMapping("/api/hello-second-world")
    fun helloSecondWorld(principal: Principal) = mapOf(
            "message" to "hello-second-world",
            "principal" to principal
    )
}