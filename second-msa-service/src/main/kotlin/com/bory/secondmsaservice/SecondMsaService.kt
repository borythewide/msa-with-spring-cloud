package com.bory.secondmsaservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication(scanBasePackages = ["com.bory.common", "com.bory.secondmsaservice"])
@EnableEurekaClient
class SecondMsaService

fun main(args: Array<String>) {
    runApplication<SecondMsaService>(*args)
}